.PHONY: all viewpdf pdf clean

MAIN         = main
OUTDIR       = ./out
TARGET       = $(OUTDIR)/main
SOURCE_FILES = $(MAIN).tex $(wildcard */*.tex)
BIB_FILES    = $(wildcard biblio/*.bib)
FIGURES      = $(wildcard */figures/*)
FLAGS        = -halt-on-error -file-line-error 

# Set the pdf reader according to the operating system
OS = $(shell uname)
ifeq ($(OS), Darwin)
	PDF_READER = open
endif
ifeq ($(OS), Linux)
	PDF_READER = xdg-open
endif

all: pdf

viewpdf: pdf
	$(PDF_READER) $(TARGET).pdf &

pdf: $(TARGET).pdf

$(OUTDIR)/%.pdf: $(SOURCE_FILES) $(BIB_FILES) $(FIGURES) these-ubl.cls
	pdflatex $(FLAGS) -output-directory=$(OUTDIR) -interaction=nonstopmode -jobname=$(MAIN) $(SOURCE_FILES)
	biber $(TARGET)
	pdflatex $(FLAGS) -output-directory=$(OUTDIR) -interaction=nonstopmode -jobname=$(MAIN) $(SOURCE_FILES) # For biber

$(TARGET).pdf: | $(OUTDIR)

$(OUTDIR):
	mkdir $(OUTDIR)

clean:
	rm -f $(TARGET).{ps,pdf,bcf,run.xml}
	for suffix in dvi aux bbl blg toc ind out brf ilg idx synctex.gz log; do \
		find . -type d -name ".git" -prune -o -name "*.$${suffix}" -print | xargs -rt rm; \
	done
	rm -rf $(OUTDIR)
