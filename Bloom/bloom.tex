\chapter{Bloom-Filter Based Memory Write Tracking for Differential Backup}
Differential backup strategies save the difference between the current state and the previous backup, to minimize the amount of data that needs to be saved.
This type of strategy can be useful in the context of intermittently powered systems, that need some form of check-pointing to preserve computation across power interruptions.
However in order to apply a differential backup strategy, the platform needs to keep track of the changes in the state of the system memory.
A simple approach to track memory writes is to use a bitmap, where each bit marks whether a section of memory has changed or not, as described in Chapter~\ref{chap:freezer}.
This approach works better when the size of each memory section is small (e.g. 8 words), giving a finer granularity to track the changes.
When size of the system memory is small enough, this fine granularity is achievable with a relatively small number of bits.
However this approach is difficult to scale to larger memories, as it requires either a large bitmap or to reduce the granularity of the differential tracking.

This chapter presents an optimized Differential backup scheme that uses approximate membership data structures, such as Bloom filters, to track the memory changes.
This approach achieves better results than a comparable plain bitmap scheme, while enabling the use with larger main memories.

\section{Introduction}%
\label{sec:bloom-intro}
In recent years the use of environmental energy harvesting has been explored as a source to power edge devices.
In particular energy scavenging from the environment can enable battery-less operation for some of these systems.
However power from the environment is usually scarce, and unpredictable in nature.
This means that this types of system are transiently powered, and need to be able to withstand power outages without loosing progress.

To enable these intermittent systems to operate across unexpected power failures, emerging Non-Volatile Memories are used to create Non-Volatile Computing systems.
One approach to enable intermittent computing is to make the whole processor non-volatile and use an NVM as the main memory.
This approach however comes with a penalty in terms of both performance and energy consumption at run-time, as NVMs are normally slower and require more energy per access with respect to SRAM, especially when it comes to memory writes.
Moreover most NVMs do not yet have the same level of endurance of SRAM, making them less suitable as a system memory replacement.

Another common approach is to use SRAM as a system memory and rely on some backup mechanism to save data on the NVM.
To execute the backup there are both static and dynamic techniques.
With static techniques such as periodic check-pointing, check-points are scheduled at compile time based on some heuristics, such as after function calls or at the end of each loop iteration as it proposed in~\cite{ransford_mementos:_2011}.
Other approaches also include the use of timers also proposed in~\cite{ransford_mementos:_2011}, and the use of stack size analysis, to place backup points in places where the size of the stack and thus the size of the backup is minimized \cite{zhao_software_2015, zhao_stack-size_2017}.

On the other end there are dynamic techniques that implement \emph{on-demand backups}.
Approaches such as Hibernus \cite{balsamo_hibernus:_2015, balsamo_hibernus_2016} try to implement a more reactive system where the backups are only executed before a power-failure, in response to a triggering event.

Contrary to static methods, approaches such as hibernus execute full memory backups, as it is difficult to optimize the backup size, when backup events are unpredictable.

To optimize backup size in an on-demand scheme, we proposed Freezere \ref{chap:freezer} \cite{pala_freezer_2020}, where a simple hardware module is used to spy the CPU's memory accesses, and track the modified memory blocks.

A similar approach is presented in~\cite{berthou_mpu-based_2020}, where instead of using a dedicated hardware component, the memory protection unit (MPU) of ARM processors is used to track dirty pages.
Similarly in~\cite{sakimura_10.5_2014} each internal register is paired with an additional dirty bit that is used to determine wich register needs to be saved.

While the use of dirty bits to implement differential backup schemes is effective, this approach is difficult to scale to larger memory sizes without losing in granularity.

In this Chapter a study of differential backup techniques using approximate membership data structures is presented.
In particular the use of bloom filters and their integration with direct-map bitmaps, to track dirty memory blocks for a differential backup algorithm is investigated.

%Bloom filters are compact data structures that using hashing can answer set membership queries.
The use of plain bloom filters, as well as bloom filters with a false positive free zone (EGH filters) \cite{kiss_bloom_2018} is analyzed.
Finally a hybrid hierarchical strategy, that uses a bitmap to track writes at a coarse granularity (page level) with a bloom filter used to track at a finer granularity (1-4 words) is presented.
For each of this presented strategies a number of parameters, such as the total number of bits, the number of keys, the level of granularity etc. are evaluated, using a memory access trace based simulation.

The result of this simulation shows that the proposed hybrid approach can significantly reduce the size of the backup with respect to a plain bitmap approach with the same number of bits as presented in~\cite{pala_freezer_2020}.
Moreover when considering larger main memory, the hybrid scheme can even achieve smaller backup size with a much reduced number of bits, respect to a Freezer approach with a fine granularity of 8 words, which would require much larger bitmap to track a relatively large main memory.



\section{Background and Related Work}
This Section presents a brief overview of the basics concept and the related works on the subject of incremental backups for transiently powered systems.
Additionally a brief and non comprehensive introduction to bloom filters and other similar data structure is given.

\subsection{Incremental Backup}
Transiently powered systems can adopt two main strategies to preserve their state across multiple power failures:
static or periodic check-pointing, and on-demand backups.
Contrary to static or periodic check-pointing, where the backup position are predetermined and On-demand check-pointing is a backup strategy that defers the execution of the backup to the moment when a power outage is signaled.
This allows for a more reactive system, and normally avoids the costs of roll-backs, as the backup is executed just before the power failure.
However this approach does not allow the same types of backup size optimization that are possible with static techniques.

Incremental or differential backup is a strategy that can be used to reduce the size of the backup in on-demand strategies.
With incremental backup the idea is that only what has been modified since the last backup needs to be saved.
This requires either computing or keeping track of the differences between the backup state and the current state of the system.
While this is common in other domains, it is more challenging to achieve with the limited computing resources and energy available to intermittent systems.

Different forms of differential backup for intermittent systems, have been presented in the literature in recent years.
In~\cite{sakimura_10.5_2014} a Non-Volatile Processor based on STT-RAM is presented.
The proposed system features hybrid non-volatile flip-flops that enable fast save and restore of the processor registers.
To avoid unnecessary backups each register is paired to an additional dirty bit, which marks weather the current volatile content needs to be saved.
This is effectively implementing incremental backups at the registers level.
One problem with this approach is that it requires exotic components such as hybrid magnetic flip-flops, and that adding an additional bit for each register incurs in a large overhead in terms of area, making this approach difficult to scale.
Moreover in the proposed method does not cover the main memory, which is fully non-volatile in the proposed NVP. 

Extending incremental backup to the main memory is more challenging but solutions have been proposed both at the hardware level and at the software/OS level.

Freezer \cite{pala_freezer_2020} is a hardware module that is able to spy the memory access of the CPU at runtime, and keeps track of the modified blocks using a bitmap.
At backup time the bitmap is used to copy from SRAM to the NVM just the modified blocks of memory.
A similar approach is presented in~\cite{berthou} where instead of a dedicated hardware component, the memory tracking is perform exploiting the memory protection unit (MPU) of ARM processors. 

With respect to Freezer, this approach is easier to adapt to available off-the-shelf micro-controllers, on the other end Freezer is a custom component designed for the task, thus it offers better granularity and lower power consumptions with respect to using the MPU.

However the use of a bit for each block of 8 words, as it is done in Freezer, is difficult to scale to larger memory sizes.
For this reason, to improve the granularity, without dramatically increasing the bitmap size, we investigate the use of hashing and approximate membership data structures such as bloom filters. 

%\begin{itemize}
%    \item freezer
%    \item register dirty bits \cite{sakimura_10.5_2014}
%    \item MPU-based incremental checkpointing  
%\end{itemize}

\subsection{Hashing and Bloom Filters}
Bloom filters are a space efficient data structure that is used to answer set membership queries \cite{bloom}.
The bloom filter offers two main operations, \emph{insertions} and \emph{queries} \cite{kiss_bloom_2018}.
In the simplest form bloom filters consist of a bit array $B$ of length $m$ that is used to represent the set, and $k$ hash functions, with $k << m$, that are used to map elements of the set to $k$ different numbers $<m$, that are used to index the bit array \cite{kiss_bloom_2018}.

To insert an element $x$ in the set, $k$ hash functions $h_1(x),\dots,h_k(x)$ are $k$ computed, and the resulting indices are used to set the corresponding bits in the array \cite{kiss_bloom_2018}.

$B[h_i(x)] \leftarrow 1, \forall i \in 1\dots k$

Figure~\ref{fig:bloom-insert} shows an example of an element \textbf{x} being inserted in a bit array \textbf{B}, using $k=2$ hash functions $h_0$ and $h_1$.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{Bloom/img/bloom_filter_2key_write}
    \caption{Insertion of \textbf{x} in a 16 bit filter \textbf{B} with two hash functions $h_0$ and $h_1$.}
    \label{fig:bloom-insert}
\end{figure}

To perform a query, i.e. to check if an element x is in the set, the k hash functions are computed, and each corresponding bit in the array is checked to be one.

$x \in S \implies B[h_i(x)] = 1, \forall i \in 1 \dots k$

If any of the checked bits is zero, then it is sure that the element $x$ does not belongs in the set $S$.
However, because of the possibility of collisions, the membership of an element cannot be determined without uncertainty.

The probability of a false positive, can be bound and controlled through the other parameters, and is the subject of many studies in the state of the art \cite{}.

Many variants of the bloom filter have been proposed in the literature, to improve performance or to adapt-it to different work-cases.

As an example in~\cite{goodrich_invertible_2011} an Invertible Bloom Lookup Table is presented, that allows to list the inserted elements.

In~\cite{kiss_bloom_2018} a bloom filter with a false positive free zone is presented.
The proposed filter uses results from the context of \emph{Combinatorial Group Testing} to build a bloom filter that, as long as the number of inserted elements is less than a number $d$, is guaranteed not to have false positives.
The proposed filter, called EGH filter, also allows to list the inserted elements.

In this work, we want to use the bloom filter to store informations about modified memory addresses.
This means that other than insertion and query, a desirable operation is to retrieve the list of inserted elements.

However because of the relative complexities of approaches such as \cite{kiss_bloom_2018} and \cite{goodrich_invertible_2011}, these filters might not be suitable for our context.
The EGH filter \cite{kiss_bloom_2018} would require the hardware implementation of complex blocks such as modulo and multiplication operations.
While the invertible bloom lookup table~\cite{goodrich_invertible_2011} would require additional memory for storing the filter.

Instead to enable a small and fast hardware implementation we explored the use of a simple \emph{``xorshift''} function \cite{marsaglia_xorshift_2003} as the hash function for our bloom filter.


\section{System Model}
In this Section we describe the system model considered for this study.
In particular we are considering the same system model considered with Freezer \ref{chap:freezer}.
We suppose that the platform is composed of a main CPU or microcontroller, with access to an SRAM memory, and with an on-chip NVM memory.
We propose the insertion of a hardware module that sits besides the CPU core, and that at run-time, spies the memory accesses.
At backup time the proposed hardware module uses the information collected at run-time to copy from SRAM to the NVM all the modified blocks of memory.
Similarly at restore time the blocks moves back the content of the backup from NVM to SRAM.

\section{Filter-Based Memory Write Tracking}
In this section we present the different architecture that we explored to implement and optimize the memory write tracking for the backup and restore controller.

\subsection{Plain Bloom}
The first architecture considered for this study is the use of a plain Bloom filter, instead of the bitmap that is used in Freezer.
While the replacement of a bitmap with the filter seems straightforward, it brings several challenges.

First of all the Bloom parameters must be chosen to reduce the false positive rate.
At the same time, because we want to implement this in a hardware module, that operates in a very constraint environment, it needs to be implemented considering things like the complexity of the hash function, and the delay to access the bit-array.

For this reason limiting the number k of hash functions can be helpful as it reduces the number of accesses that need to be done to the bit array.
Another approach is to divide the bit array into parallel register blocks, so that multiple read accesses can be executed in parallel.
This however is still limited by the possibility of conflicts, as two or more hash functions could end up producing indices that access the same register block, creating contentions.

Another problem with the plan substitution of the bitmap with the bloom filters, is that it does not allow for a fast retrieval of the modified words.
In fact for a direct map bitmap every bit corresponds to a block of words, this allows to quickly retrieve the address of the modified blocks, simplifying the backup procedure.
On the other hand there is no easy way to invert the function of the bloom filter, and extract the addresses from the bit vector.

This means that to perform the backup, the whole address space must be checked against the filter and only those addresses that results in the filter should be considered for the backup.
This adds overhead in the backup procedure by adding unnecessary checks in the filter, moreover it also increases the chance of collisions, as the whole universe (the address space) must be tested.

To mitigate these shortcomings, registers could be added to the hardware block, to hold the boundaries and limit the search space.
As an example the simplest case would be to add two registers, one holding the minimum value for the addresses, the other for the maximum value.
Then the search in the filter could be limited within these two boundaries.
With the addition of more of these registers the address space could be further divided into more regions, and only addresses within those regions would be checked at backup time.

This registers could be filled automatically by the hardware, or their value could be set by the programmer.

\subsection{Parallel Bloom}
Another option to improve the search time during the backup operation, is to use the available number of bits to implement multiple bloom filters, that could be accessed in parallel.
The filters could share the same hash functions, which would be computed only on the lower bits of the address while the higher bits would be used to identify the region and thus the corresponding filter.
Each of these filters maps to a different region of the address space.
This allows to check the same low bits of the address in multiple regions at the same time, and effectively dividing the size of the universe, with respect to the plain bloom-filter approach.

The main drawback of this approach is that by dividing the total available number of bits between multiple bloom filters, the chances of collisions within those filters increases.
As the objective is to embed the tracking hardware in the SoC, only a limited number of bits is available for the bloom filters, and dividing them between multiple bloom filters makes them smaller and less effective.

Moreover depending on the access patterns and on the locality of the application, the memory accesses might be confined to only few regions in between backups.
When the accesses hits mainly one region, the corresponding bloom filter would be saturated, while the others will remain mostly empty.
This means that the bits that are dedicated to the other regions are actually wasted.

To counter this effect, one approach could be to use a different mapping between the addresses and the corresponding bloom filter, instead of just dividing the address space in consecutive regions.

%\subsection{Hierarchical Bloom}
%TODO: describe

\subsection{Hybrid Hierarchical Bloom-Freezer}
The last architecture that we propose is a combination of the bitmap approach of Freezer as described in Chapter~\ref{chap:freezer} \cite{pala_freezer_2020}, and a bloom filter.
The idea is to use the bitmap to divide the address space in contiguous regions, or pages, each bit of the bitmap correspond to a different region, and it is set if any of it's words is modified in between backups.
Then a single bloom filter is used and shared between the different regions, to mark the modified blocks.
The blocks are the basic units of memory that are tracked by the system for the backup.
A block could consist of few contiguous words (e.g. two or four words) or of a single word, if word level tracking is desired.

The main idea behind this approach is to use the bitmap to constrain the search space, as only the marked regions needs to be checked.
This is a simpler alternative to using boundary registers with a plain bloom filter, where pairs of registers hold the value of the minimum and maximum address determining the search regions.
Instead the bitmap is used to mark fixed, direct mapped, and aligned regions.
While this does not offer the flexibility of tracking unaligned address ranges, it allows to track more regions with the same number of bits with respect to using those bits for the boundary registers.

With the hybrid bloom-freezer approach, at run-time when a store is executed, the high bits of the address are used to access the region bitmap and set the corresponding bit to one.
In parallel, the full block address (i.e. including the region bits) is used as input of the hash function, to compute the indexes for the bloom bit-vector, and the corresponding bits are also set to one.

At backup time, the information on the regions stored in the bitmap is used to limit the search through the bloom filter, to those regions that have their bit set.
For each marked region, all the block addresses in the region must be checked against the bloom filter.
This means that for all the blocks in a marked region, the hashes must be computed and the bloom bit-vector must be access at the corresponding indices.
Only if all those bits are set the block is copied from SRAM to the NVM.

As it can be deduced, this operations, especially checking every block in a region, can slow down the backup operation.
For this reason a careful balance must be searched, when deciding how many bits should be allocated for the region bitmap, and how many for the bloom bit-vector.
In particular increasing the size of the region bitmap, leads to a higher number of regions, because the memory size is the same this means that each region will be smaller, reducing the number of block addresses that must be checked for each region.
This also helps in situations where the access pattern is sparse, as the overhead of checking a region with few blocks to backup is reduced.
On the other hand, increasing the size of the region bitmap, leaves less bits for the bloom filter, reducing its accuracy and increasing the possibility of false positives, and so increasing the size of the backup.

In the following Section some of these trade-offs are explored, and the results of experiments with the different parameters are presented.

\section{Results}
This section presents the result of a simulation where different filter strategies are compared with different parameters.
Additionally the results of a preliminary hardware implementation are reported.

\subsection{Simulation Setup}
In order to test the performance of the bloom-filter based approaches, and compare them with the bitmap based approach such as Freezer, parametrizable models of the different strategies have been implemented in \emph{c++}.
The simulation takes a memory access trace as it's input, while the different filters to be compared are all hard-coded in an array and embedded in the program at compile time.

The simulation proceeds by reading lines from the trace file.
Each line contains three fields: a time-stamp expressed in clock cycles, indicating when the current memory access was executed.
The next field is the type of memory access, that can be \emph{'L'} for load or \emph{'S'} for store accesses.
The last field contains the memory address.

The clock cycle information is used to divide the execution in intervals.
Each interval is supposed to be an uninterrupted period of execution, at the end of which there is a power failure.
For these simulations fixed size intervals have been considered, which means that power failures happen always every $N$ clock cycles.
The interval size is a parameter of the simulation.
Values of the interval size ranging from 100 thousands to 1 million clock cycles have been tested.

Another parameter of the simulation is the total number of bits available.
This number gives the total size of the bitmap for a freezer approach, while for hybrid bloom-freezer strategies the total number of bits must be divided between the bloom and the freezer parts. 
For this reason several configurations were tested, with different ratio of the total number of bits allocated to the freezer part, in particular ratios of $1/2$, $1/4$ and $1/8$ were tested.

When a store is encountered all the methods in the filter array are updated, i.e. inserting the address value in the bitmap for Freezer-like strategies, or updating the bloom filter for filter-based strategies.

When a memory access that belongs to the next interval is reached the backup procedure is triggered for all methods.
This means that for each method, the number dirty addresses is counted.
As a comparison a \emph{Set} strategy is also implemented, this is a strategy that uses a perfect set (i.e. without collisions), to keep track of the actual number of modified addresses.
To implement such a strategy using a bitmap to mark modified addresses, the bitmap would need to have one bit for each word in the main memory.


\subsection{Simulation Results}
In Figure~\ref{fig:backup_vs_iv} the average backup size with respect to the interval size is shown, for a few selected strategies.
In Figure~\ref{fig:backup_vs_iv} the average backup size is normalized with respect to the \emph{Set} strategy average backup size.
For all the hybrid bloom-freezer strategies (\emph{XBF-}) in Figure~\ref{fig:backup_vs_iv}, the total bit count is 4096.
As it can be seen in the figure the average backup size of the hybrid strategies is much closer to that of a perfect \emph{Set} strategy.
On the other hand using Freezer with $4096$ bits (\emph{Freezer} in Figure~\ref{fig:backup_vs_iv}) results in a significant increase in the backup size, which gets between $2\times$ and $1.8\times$ that of the perfect \emph{Set} strategy.
Figure~\ref{fig:backup_vs_iv} also shows the \emph{Freezer-B8} strategy, this is a Freezer strategy where the number of bits is not limited to 4096.
Instead the size of the bitmap is set so that each bit can track blocks of 8 words.
With the benchmark utilized for this simulation, the memory size is 1Mb, i.e. 256K words, thus in order to implement a \emph{Freezer-B8} strategy, a bitmap with 32Kbit would be necessary, making this approach much more expensive.
As it can be seen from the Figure, even with the increased number of bits, the hybrid approaches still outperform the simple Freezer approach.

\begin{figure}[ht]
\begin{center}
    \includegraphics[width=.8\linewidth]{Bloom/img/norm_avg_backup_size_4kbit}
\end{center}
\caption{Average backup size in function of the Interval Size.}
\label{fig:backup_vs_iv}
\end{figure}


\subsection{Hardware Model}
To provide an overview of the potential hardware requirements of implementing a hybrid bloom-freezer approach (\emph{BloomFreezer} module), an unoptimized high-level synthesis model have been implemented and synthesized.
Additionally a similar model of a Freezer approach (\emph{Freezer} module) has also been implemented and synthesized to provide a comparison.
Both models have been synthesized using Cadence Stratus HLS, with the default 45nm technology node available with the tool.

Figure~\ref{fig:hw_map_freezer} and \ref{fig:hw_map_bloom} show the tree map view of the hardware resources of the Freezer and Bloom-Freezer module after the high level synthesis process.
In this view the size of the components is proportional to the expected area.
This view only gives a high level approximation of the area, as the final result in terms of area and resource utilization can be changed (normally improved) by the logic synthesis and step, and finally during the physical implementation phase.
However the Figures are useful as they give an overview of the area breakdown for each component and allow us to compare the two implementations.
As it can be seen for both modules the bulk of the area is occupied by the bit-array used for the bitmap in case of the \emph{Freezer} module, and for both the freezer-bitmap and the bloom filter in the \emph{BloomFreezer} module.
The rest of the area is divided between few registers, mainly the counters for looping through the dirty words, and the register for to output the sram addresses, and few other resources and multiplexers.
The control logic is also indicated in the figure but it only occupies a small portion of the area.

As it can be seen from the figures the area of the two modules is mainly dependent on the total number of bits, however, due to the separation of the bits into two arrays for the \emph{BloomFreezer} module there is a small overhead.
Additionally the BloomFreezer module also requires a few more counters due to the slightly more complicated looping algorithm.
The BloomFreezer module area is also dependent on the number of read and write ports added to the bloom bit-array.
In fact depending on how-many bits are read and written in the bloom filter for each insertion/query, an additional read and write port is necessary.

An alternative to increasing the number of ports, could be to serialize the accesses, introducing an overhead in the latency of each insertion and query operation, and ultimately impacting the throughput.
A different approach could be to divide the bit-array into multiples banks to allow multiple parallel reads and writes, this however would require collision resolution logic for the cases when a query or a write need to access two locations in the same bank.

\begin{figure}
\begin{center}
    \includegraphics[width=.5\linewidth]{Bloom/img/category_tree_map}
\end{center}
\caption{Tree-map view of the \emph{BloomFreezer} module.}
\label{fig:hw_map_bloom}
\end{figure}

\begin{figure}
\begin{center}
    \includegraphics[width=.5\linewidth]{Bloom/img/tree_map_category}
\end{center}
\caption{Tree-map view of the \emph{Freezer} module.}
\label{fig:hw_map_freezer}
\end{figure}

As it can be expected the area of both the \emph{Freezer} and \emph{BloomFreezer} modules scales proportionally to the overall bit number of the module.
In Figure~\ref{fig:area_bf} and Figure~\ref{fig:area_freezer} the combinational and sequential area breakdown, and the scaling with respect of the total number of bits is shown.
Figure~\ref{fig:area_bf} also shows different configurations with the same total number of bits, but with a different split between the direct mapped array (the freezer part) and the bloom filter array.
The bit array organization tested were:

\begin{itemize}
   \item $1/2$ split between the direct mapped part and the bloom filter
   \item $1/4$ of the bitst for the direct mapped array, and $3/4$ for the bloom filter
   \item $1/8$ of the bitst for the direct mapped array, and $7/8$ for the bloom filter
\end{itemize}

\begin{figure}
\begin{center}
    \includegraphics[width=\linewidth]{Bloom/img/Capture_ls_bloom_freezer}
\end{center}
    \caption{\emph{BloomFreezer} modules area breakdown after logic synthesis.}
\label{fig:area_bf}
\end{figure}

\begin{figure}
\begin{center}
    \includegraphics[width=\linewidth]{Bloom/img/Capture_ls_freezer}
\end{center}
\caption{\emph{Freezer} modules area breakdown after logic synthesis.}
\label{fig:area_freezer}
\end{figure}


\begin{table}[ht]
    \centering
    \caption{Logic synthesis results summary for \emph{BloomFreezer} modules.}
\begin{tabular}{llrllllr}
\hline
FBits & BBits &   Worst & Combinational & Sequential &  \# FFs & Total   &  Total \\
{}    & {}    &   Slack & Area          &       Area &  {}     &  Area   &  Power \\
\hline
512  & 512   &   7.05 &          4,908 &      6,300 &   1,150 &  11,462 &   0.08 \\
256  & 768   &   7.39 &          5,687 &      6,300 &   1,150 &  12,196 &   0.08 \\
128  & 896   &   7.60 &          6,677 &      6,301 &   1,151 &  13,180 &   0.07 \\
\hline
1024  & 1024 &   7.04 &          8,531 &     11,898 &   2,173 &  20,819 &   0.09 \\
512  & 1536  &   7.07 &         11,076 &     11,904 &   2,174 &  23,311 &   0.11 \\
256  & 1792  &   7.29 &         11,632 &     11,904 &   2,174 &  23,848 &   0.11 \\
\hline
2048  & 2048 &   7.09 &         15,564 &     23,179 &   4,235 &  39,451 &   0.14 \\
1024  & 3072 &   6.45 &         20,481 &     23,106 &   4,222 &  44,184 &   0.39 \\
512  & 3584  &   6.41 &         22,416 &     23,111 &   4,223 &  46,073 &   0.48 \\
\hline
\end{tabular}
    \label{tab:bloom_ls}
\end{table}


\begin{table}
    \centering
    \caption{Logic synthesis results summary for \emph{Freezer} modules.}
\begin{tabular}{lrllllr}
\hline
Bits    &  Worst & Combinational & Sequential &  \# FFs & Total   &  Total \\
{}      &  Slack & Area          & Area       &         & Area    &  Power \\
\hline
1024   &    7.26 &         2,203 &      6,262 &   1,143 &   8,738 &   0.06 \\
2048   &    7.16 &         3,689 &     11,937 &   2,181 &  16,120 &   0.08 \\
4096   &    6.88 &         6,715 &     23,181 &   4,235 &  30,819 &   0.12 \\
\hline
\end{tabular}
    \label{tab:freezer_ls}
\end{table}

As it can be seen in Figure~\ref{fig:area_bf} there is a slight overhead when the number of bits is not divided in half between the direct mapped and the bloom arrays.
The data reported in Figure~\ref{fig:area_bf} and Figure~\ref{fig:area_freezer} are taken after the logic synthesis step, executed with the Genus synthesis tool.
The area and components report after the logic synthesis step show an improvement with respect to the approximated results reported after the high-level synthesis step.
Moreover for the logic synthesis, the \emph{low\_power} options were enabled.
This option brings several advantages, in particular it helped achieve a smaller combinational area by replacing the multiplexers in front of the flip-flops, with clock-gated flip-flops.

The summary result of the logic synthesis for the \emph{BloomFreezer} modules, with different direct map and bloom bit array size (\emph{FBits} and \emph{BBits} respectively) are reported in Table~\ref{tab:bloom_ls}.
Table~\ref{tab:freezer_ls} instead reports the logic synthesis data for three configurations of the \emph{Freezer} module.
As it can be seen from the two tables there is an overhead with the \emph{BloomFreezer} modules with respect to a \emph{Freezer} module with the same number of bits.
The area difference is mainly due to the additional circuitry required to address two separate register banks.
This overhead could be reduced by implementing the \emph{BloomFreezer} module with a unified memory block or register bank, shared between the bloom filter and the bitmap.
This way the multiplexing and decoding circuitry would be equivalent to that of the \emph{Freezer} module, but it would not be possible to access in parallel both the bitmap and bloom filter arrays.

Overall we consider the area overhead introduced by \emph{BloomFreezer} a small price to pay for the reduced backup size, when dealing with large main memories.
Moreover this area increase could be reduced even more, as neither the \emph{BloomFreezer} and the \emph{Freezer} modules presented in this Chapter, are optimized and complete designs.
As an example, both modules lack the implementation of the restore state machine, which would be the same for both modules, and would reduce the percentage difference in area.

\section{Conclusion}
Intermittently powered systems need to preserve the state of the system, even when facing unpredictable power failures.
In the previous chapters we presented techniques for on-demand differential backup using a bitmap to track modified sections of memory. 
The bitmap approach works best when the size of each memory section is relatively small (like 8 words sections).
However this is difficult to scale to larger memory sizes, as the size of the bitmap would also increase significantly.

To overcome this limitation this Chapter explores the use of bloom filters, in combination with bitmaps, to track larger memories, with a fine granularity, but without requiring a large number of bits. 
The approach that lead to the best results is the \emph{Bloom-Freezer} approach (\emph{BF}), that use of a bitmap to track large memory sections (i.e. pages), combined with a single bloom filter, used to track the single memory words.
This approach limits the collisions, and the search space, of the bloom filter, to only the memory sections marked in the bitmap.
The technique was able to achieve, in our simulations, significantly better results than the \emph{Freezer} approach described in Chapter~\ref{chap:freezer}, with the same number of bits.
With the \emph{BF} strategy resulting in a backup size that is between $50\%$ and $65\%$ that of the \emph{Freezer} approach, depending on the lenght of interval between two consecutive backups.

This Chapter also presents an unoptimized hardware implementation of both the \emph{BF} and the \emph{Freezer} strategies, realized using the same synthesis tools and with the same technology library.
The result of the hardware synthesis shows that the additional complexity introduced by the \emph{BF} algorithm does results in an increased area, however the area is overall still dominated by the register banks, and thus by the number of bits.

In summary, this Chapter explores a path for the optimization of on-demand differential backup, for intermittently powered systems.
The proposed strategy could enable the use of more capable systems with larger main memory, in transiently powered scenarios.
While this is an early exploration, the use of aproximate set membership data structures seems to be a promising route to achieve fine granularity memory writes tracking, without a large overhead.
Possible future explorations, might include the use of a more complex hierarchy for the dirty memory tracking, and the use of more complex data structures such as cuckoo filters or more advanced variations of a bloom filter \cite{cuckoo}. 
