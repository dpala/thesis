\chapter{Non-Volatile Memories}
\section{Introduction}

\section{Phase Change Memory}
The basic working principle of Phase Change Memories (PCMs) is to exploit the large difference in resistance, between the amorphous and crystalline state of so called \emph{Phase Change Materials} to represent the information.
This idea has been first explored in the 1960s \cite{ovshinsky_reversible_1968, burr_phase_2010, hong_emerging_2014}, however thanks to more recent advances in the field of phase change materials, it has received a renewed interest from both academia and industry \cite{rizk_demystifying_2019}. 
PCM is one of the most mature among the so called \emph{``Emerging Non-Volatile Memories''} \cite{rizk_demystifying_2019}, with many prototypes and devices that have been demonstrated and with some devices already available on the market.

\subsection{SET and RESET write operations}
The PCM memory cell is based on the property of chalcogenide materials to exist in either the amorphous or the crystalline state at room temperature.
These two states show a large difference in resistance, typically between $100\times$ and $1000\times$ but that can be up to 4 orders of magnitude \cite{burr_phase_2010, yu_emerging_2016}.
The crystalline phase is associated to a low resistance state \textbf{LRS}, while the amorphous state has a relatively high resistance \textbf{HRS}.

Figure \ref{fig:pcm_temp} shows the programming pulses required to switch from the \emph{RESET} amorphous \textbf{HRS} state, to the crystalline \textbf{LRS} \emph{SET} state, and vice-versa.  

The switching between these two phases is thermally induced through Joule heating.
In particular to transition from the crystalline to the amorphous phase, a very short pulse of high current is applied to the device.
This rapidly heats the chalcogenide material above the melting point.
The short duration of the current pulse allows the material to cool down quickly and settle to an amorphous state.

To obtain the opposite transition from the amorphous \emph{HRS} to the crystalline \emph{LRS} phase, a current pulse with lower intensity is applied for a slightly longer period of time.
This current pulse heats the memory element above the crystallization temperature, and last long enough to allow the material to crystallize.

\begin{figure}
    \center
    \includegraphics[width=.5\linewidth]{NVM/img/pcm_temp}
    \caption{SET and RESET pulses.}
    \label{fig:pcm_temp}
\end{figure}

As it can be seen from Fig. \ref{fig:pcm_temp} the \emph{SET} transition, shown in blue on the Figure, from amorphous to crystalline phase is the limiting factor in terms of write time for this types of memory.
On the other hand the \emph{RESET} transition, shown in red in the Figure, is faster but it imposes the limit on the peek power, due to the required high current.


\subsection{PCM Cell}
The typical PCM cell structure is shown in Fig. \ref{fig:pcm_cell}.
The cell has a simple structure composed of two electrodes that sandwich the heating element and a thin layer of the phase-change material, which is the memory element itself.

There are several materials that have been identified, that have the properties of the two stable state at room temperature.
PCMs cells are commonly based on the so called \emph{GST} materials like $Ge_2 Sb_2 Te_5$ \cite{yu_emerging_2016, hong_emerging_2014, xue_emerging_2011}, compounds of \emph{Germanium} \emph{Antimony} and \emph{Tellurium}.
These and others materials, such as silver and indium doped $Sb_2 Te$ (AIST), have attracted the interest in the research community, due to their sub-$100 ns$ crystallization times \cite{burr_phase_2010, hong_emerging_2014}.

Figure \ref{fig:pcm_cell} shows the typical PCM cell with a so called ``mushroom'' structure.

Other structures have been proposed and are summarized by Fujisaki in \cite{fujisaki_review_2013}.
Moreover as reported by Yu and Chen in \cite{yu_emerging_2016}, the general trend seems to be to move to a so called \emph{pillar} structure, which improves write currents by better confining the heat dissipation. 

\begin{figure}
    \center
    \includegraphics[width=.5\linewidth]{NVM/img/my_pcm_mushroom_cell}
    \caption{Typical mushroom type memory cell structure of a PCM memory.}
    \label{fig:pcm_cell}
\end{figure}

%\begin{figure}
%    \center
%    %\includegraphics[width=.5\linewidth]{NVM/img/fujisaki_pcm_structures}
%    \caption{Different cells structures as reported in \cite{fujisaki_review_2013}}
%    \label{fig:pcm_structures}
%\end{figure}

The memory cells in PCM arrays can be as simple as a one transistor one resistor (1T/1R) devices, with the resistive memory element and an access transistor.
The access transistor can either be a FET or a BJT, with BJTs being more promising for both speed and scaling \cite{lee_architecting_2009}.

However because the transistor can be the limiting factor to scalability \cite{derchang_kau_stackable_2009}, alternative selector devices have been proposed, such as a diodes \cite{yu_emerging_2016} or Ovonic-threshold-switch (OTS),  where the selector is in series with the memory element and occupies the same footprint \cite{hong_emerging_2014, derchang_kau_stackable_2009}. 
As an example, a \emph{Cross-Point} PCM memory was demonstrated by DerChang et al. in \cite{derchang_kau_stackable_2009}, where they integrated vertically the PCM and the OTS, as shown in Fig. \ref{fig:pcm_ots_stack}. 

\begin{figure}
    \center
    \includegraphics[width=.45\linewidth]{NVM/img/pcm_ots_stack}
    \caption{Vertical stacking of PCM and OTS as presented in \cite{derchang_kau_stackable_2009}}
    \label{fig:pcm_ots_stack}
\end{figure}


\subsection{Characteristics}
%The promise of good scalability, together with the maturity of the process and it's compatibility with standard CMOS, make PCM an interesting memory and a good candidate to replace FLASH or even DRAM.
The main advantages of PCM technology are the maturity of the process, it's compatibility with CMOS, and the promise of good scalability and high density.
The write speed is limited by the \emph{SET} operation, which, as shown in Table \ref{tab:comparison} can be as low as $20 ns$, even though more common values are above $100 ns$ \cite{lee_architecting_2009}.
The \emph{RESET} transition on the other hand can be relatively fast, usually less than $60 ns$ \cite{lee_architecting_2009}, or even sub $10 ns$ as demonstrated in \cite{derchang_kau_stackable_2009}.
However the transition to an amorphous state also requires a much higher current, that can be up to two times as much as the current for the \emph{SET} transition.
%DONE: add more details
However because of its fundamental working principle the technology shows slightly worse performance, especially in terms of energy per bit which is in the order of $\sim10 pJ/b$ for write operations, than other emerging NVMs as summarized in Tab. \ref{tab:comparison}. 

%TODO: values are closer to 10^5
Additionally the endurance is also limited, with values ranging from $10^5$ to $10^9$ which is an improvement with respect to FLASH memories but it is still limited when compared to DRAM or other NVMs such as STT-MRAM.

%    \begin{itemize}
%      \item Use phase change of \emph{Chalcogenide} material to retain information.
%      \item \textbf{SET} operation to turn to \emph{crystalline} phase \textbf{LRS} ("1")
%      \item \textbf{RESET} operation to turn to \emph{amorphous} phase \textbf{HRS} ("0")
%      \item large on/off resistance ratio (x100-x1000) 
%      \item high write current.
%    \end{itemize}

\section{Magnetic RAM}
In this work we use Magnetic RAM or \textbf{MRAM} to indicate a family of memories that use a device called \emph{Magnetic Tunnel Junction} \textbf{MJT} to store the information.
%Magnetism has been exploited in the past to store information, 
The working principle of the MTJ which is common to many different types of MRAMs, is to store the information as the direction of the magnetization of two ferromagnetic layers.
The different types of \emph{MRAMs} use different techniques and physical phenomenons to switch the direction of one of the two layers of the MTJ.
These techniques are used to put the two magnetic directions into either a \emph{parallel} or an \emph{anti-parallel} state.
These two configurations are used to encode the information, as they show a significant difference in resistance through a phenomenon that is called \emph{``tunnel magnetoresistance''} (TMR) \cite{moodera_large_1995}.

%    \begin{itemize}
%      \item Memories based on the \emph{magnetic tunnel junction} \textbf{MTJ}
%      \item MTJ: two ferromagnetic films separated by thin insulator (MgO)
%      \item Magnetization direction of one layer is \emph{fixed} the other is \emph{free}
%      \item Information is stored in the magnetic orientation of the free layer
%      \item Two resistance states: \emph{parallel}, \emph{antiparallel}
%      \item Small on/off resistance ratio (x2) 
%    \end{itemize}

\subsection{Magnetic Tunnel Junction}
The Magnetic Tunnel Junction is the basic building block of a variety of MRAMs.
Conceptually this is a simple device, composed of a thin tunneling dielectric (e.g. $MgO$), sandwiched by two ferromagnetic layers.
One of the ferromagnetic layers is called the \emph{``fixed''} or \emph{``pinned''} layer, because it holds a fixed magnetic direction.
On the other hand the other layer is called the \emph{``free''} layer, as its magnetic direction can be changed.

This creates two possible configurations, one where the direction of the magnetization of the two layers is parallel, and one in which they have opposite direction and are anti-parallel.
Figure \ref{fig:mtj} depict a conceptual view of the MTJ structure.

When the magnetization of the two layers is parallel, the resistance of the MTJ is smaller than when they are anti-parallel.
This difference in resistance is due to the TMR effect and can be as high as $200-300 \%$ \cite{ando_non-volatile_2017}.

\begin{figure}
    \center
    %\includegraphics[width=\linewidth]{NVM/img/}
    \caption{MTJ structure}
    \label{fig:mtj}
\end{figure}

% TODO: specify chapter for @book and \cite[Chapter~5]{foo} for a monograph.
% TODO: specify chapter for @incollection for a book in which each chapter has a different author. Then the relevant fields are booktitle= and title=;

% @InCollection{Sentieys2004,
% author = {R. David and S. Pillement and O. Sentieys}, title = {{Energy-Efficient Reconfigurable Processsors}}, booktitle = {{Low Power Electronics Design}},
% editor = {C. Piguet},
% chapter = {20},
% publisher = {CRC Press},
% series = {Computer Engineering, Vol 1},
% month = aug,
% isbn = {0-8493-1941-2},
% year = 2004
% }

\subsection{Field Induced Magnetic Switching MRAM}
The first generation of MTJ based MRAMs used \emph{Field Induced Magnetic Switching} to rotate the magnetization direction of the free layer.
One type of memory that uses this mechanism is the so called \emph{Toggle MRAM} \cite{engel_4-mb_2005}, which is a relatively mature technology with products that are available in the market, manufactured by companies such as Freescale and Everspin.

The typical memory cell for Toggle MRAMs is a one transistor one MTJ cell, as depicted in Figure \ref{fig:toggle}. 
As shown in the figure the cell uses two write wires that carry a pulsed current, this generates a field that induces the switching in the MTJ.

\begin{figure}
    \center
    \includegraphics[width=.7\linewidth]{NVM/img/toggle_mram}
    \caption{Toggle MRAM cell from \cite{engel_4-mb_2005}}
    \label{fig:toggle}
\end{figure}

The read operation is performed by activating the access transistor and letting a sense current flow through the device.

This type of memory can be arranged in a cross bar array, it has good performance with vendor such as Everspin producing devices with $35 ns$ symmetric read/write access and claiming unlimited endurance and long retention times \footfullcite{noauthor_toggle_nodate}. 
However due to the additional wires, and the high current required for the switching (in the order of few mA), which does not reduces with scaling \cite{andre_st-mram_2013, jovanovic_comparative_2015}, this type of MRAM are difficult to scale below 90 nm \cite{senni_exploring_2016}.
For this reason both the research community and the industry has researched other switching techniques to overcome the scaling problems of Toggle MRAM.

\subsection{TAS-MRAM}
\textbf{TAS-MRAM} stands for \emph{Termally Assisted Switching MRAM}.
As the name implies this type of MRAM uses heat to favor the switching of the MTJ state.
This technique is also a form of field induced switching, however instead of requiring two high currents lines to induce the switching, it requires a single high switching current to generate the magnetic field \cite{hong_emerging_2014, jovanovic_comparative_2015}.
Additionally a small current is also used to heat the MTJ before the switching current is activated.
The structure and switching mechanism of TAS-MRAM is illustrated in Figure \ref{fig:tasmram}.

\begin{figure}
    \center
    \includegraphics[width=.9\linewidth]{NVM/img/senni_tasmram}
    \caption{TAS-MRAM cell switching from \cite{senni_exploring_2016}}
    \label{fig:tasmram}
\end{figure}

The stack of a \emph{TAS-MRAM} MTJ is slightly different as it requires the replacement of the free layer, with an exchange bias layer, obtained by coupling the free layer with an anti-ferromagnetic layer \cite{hong_emerging_2014}. 
This layer has a lower blocking temperature than the one used to lock the \emph{reference} layer, and it is used to pin to the free layer.

As shown in Fig. \ref{fig:tasmram}, the switching process requires 3 major steps: \emph{heating}, \emph{switching} and \emph{cooling}.
To begin the switching process the access transistor is switched on, allowing a current to flow through and heat the device.
Once it is heated above the free layer blocking temperature, the free layer magnetization can be changed through the application of an external field.

To terminate the write operation the heating current is switched off, while the external field is kept also during the cooling of the device.

The addition of the blocking layer improves the data reliability, by improving write selectivity and thermal stability, while also reducing the power consumption with respect to toggle-MRAMs \cite{hong_emerging_2014, jovanovic_comparative_2015}.
One of the drawbacks of this write scheme however, is that it increases the write time, due to the time required by the heating and cooling processes.
Additionally even though the write current and power is reduced when compared to field switching MRAM, writes still require a significant amount of current (in the order of few mA) \cite{senni_exploring_2016, jovanovic_comparative_2015}.

\subsection{STT-MRAM}
\textbf{STT-MRAM} is a type of magnetic memory that exploits the \emph{Spin-Transfer Torque} effect to switch the direction of the free layer.
This effect allows the storage layer to be written only by the current flowing through the MTJ, without needing the strong external magnetic field, like the previous types of MRAMs \cite{hong_emerging_2014, senni_exploring_2016}.
The basic idea behind this type of switching, is to let a current carrying spin-polarized electrons, flow through the device.
These spin polarized electrons exert a torque on magnetization of the free layer \cite{van_den_brink_spin-hall-assisted_2014}.
When this current is above a certain threshold, called switching current, it can change the magnetization direction of the free layer.
The amplitude of this current depends on the physical characteristics of the MTJ such as materials and structure, but it is also heavily related to the amount of time the current is applied to the MTJ \cite{li_nonvolatile_2017}.

An advancement over the standard \emph{In-Plane} MTJ based \emph{SST-RAM} is is represented by the \emph{Perpendicular} MTJ based STT-RAM.
%In perpendicular MTJ, represented in Fig. \ref{fig:perpendicular}, the magnetization direction is perpendicular to the surface of the device. 
In perpendicular MTJ, the magnetization direction is perpendicular to the surface of the device. 
This type of structure is promising as it shows better thermal stability and reduced switching current, which should enable better scaling \cite{hong_emerging_2014, van_den_brink_spin-hall-assisted_2014}.

%\begin{figure}
%    \center
%    %\includegraphics[width=.9\linewidth]{}
%    \caption{}
%    \label{fig:perpendicular}
%\end{figure}

STT-MRAM promise good scalability as the switching current is reduced proportionally by scaling the area of the MTJ \cite{andre_st-mram_2013}.
%DONE: terory -> theory
The cell size can in theory be as small as $6 F^2$ \cite{ando_non-volatile_2017}.
Products with different capacities have been demonstrated and commercialized, going from a 64Mb memory produced on a 90 nm CMOS node \cite{slaughter_high_2012}, up to the more recently presented 1Gb memory, produced on a 28 nm node \cite{aggarwal_demonstration_2019}. 

STT-MRAM one of the more promising type of memory among the emerging NVMs, with the fastest access time, in the order of ns, and the best endurance ($\sim 10^{15}$).
For these reasons STT-MRAM is seen as a good candidate to replace DRAM or even SRAM in some applications.

STT-MRAM however, still faces some problems that have to be addressed before it can truly replace DRAM or SRAM.
STT-MRAM has a relatively small on/off resistance ratio, which complicates the design of the sensing circuitry \cite{chen_review_2016, rizk_demystifying_2019}, and is also affected by variability, posing a problem in the design of highly integrated memories \cite{fujisaki_review_2013}.
Moreover the fabrication and integration with the CMOS process also poses some problems related to the use of exotic materials and to the required temperatures \cite{yu_emerging_2016, chen_review_2016}. 

%\subsubsection{Multi-level Cell}
%\cite{li_nonvolatile_2017}

%\subsection{SOT-MRAM}
% Mention new memory 

\section{Resistive Memories and Memristors}
While both MRAM and PCM exploits a change in resistance to store the information, the term Resistive RAM \textbf{ReRAM} or \textbf{RRAM} refers to a particular class of devices that store the information as the resistance change across a dielectric solid-state material.
These types of two terminal devices are also known as \emph{memristors}, and were first defined theoretically by Chua \cite{chua_memristor-missing_1971} in 1971.
However it was only in 2008 that HP Labs demonstrated the appearance of memristance in some nano-scale systems \cite{strukov_missing_2008}.
Since then the research has progressed and several types of RRAM devices have been demonstrated, with different types of materials.

\begin{figure}
    \center
    \includegraphics[width=.7\linewidth]{NVM/img/cbram_filament}
    \caption{The formation ($A \rightarrow D$) and rupture ($E$) of the conductive filament in a CBRAM device \cite{valov_electrochemical_2011}.}
    \label{fig:filament}
\end{figure}

The basic structure of an RRAM device is a \emph{metal-insulator-metal} structure, where a thin oxide film or a solid-state electrolyte is sandwiched between two metal electrodes \cite{fujisaki_review_2013, li_nonvolatile_2017, rizk_demystifying_2019}.
The switching mechanism exploits the formation or rupture of a conducting filament, which is induced in the insulating film by an applied voltage, as shown in Figure \ref{fig:filament} \cite{valov_electrochemical_2011, ando_non-volatile_2017}

The resistance switching can happen either in a unipolar or bipolar fashion, depending on the type of RRAM device.
Figure \ref{fig:rram_switching} shows the switching $I-V$ curve, in both unipolar and bipolar mode, for a HRS to a LRS transition, called \emph{Set}, and the opposite transition from a LRS to a HRS called \emph{Reset}, as reported in \cite{yu_resistive_2016}.

%TODO: explain easier cross-point integration
Devices that operates in unipolar mode, such as many oxide based RRAM, makes it easier to achieve high integration with cross-point cells array, using a diode as a selecting device \cite{fujisaki_review_2013, ando_non-volatile_2017}.
However bipolar mode is considered more promising as it shows improved stability, requires lower currents and better endurance.
Moreover more advanced structures, with two combined resistive elements, could enable large passive cross-bar arrays without the need for a selector device \cite{valov_electrochemical_2011, fujisaki_review_2013}.

Before being able to perform these resistance switching, many RRAM devices, require to undergo a procedure called \emph{forming}.
This is a one-time process, that is similar to the \emph{Set} operation, but with higher voltages that need to cause a soft breakdown of the Metal-Oxide-Metal structure \cite{akinaga_resistive_2010}.
The voltage required for the forming process used to be relatively high, and was one of the main disadvantage of early resistive memories.
%TODO: mitigate 
However technological advancement and the shrinking of the oxide thickness, have made possible to eliminate the requirement for the forming process \cite{yu_resistive_2016}.

\begin{figure}
    \center
    \includegraphics[width=.7\linewidth]{NVM/img/rram_switching_crop}
    \caption{RRAM switching $I-V$ characteristics in unipolar and bipolar mode, from \cite{yu_resistive_2016}.}
    \label{fig:rram_switching}
\end{figure}

There are two main families of RRAM devices, based on two different types of switching: oxide based RRAM or \textbf{OxRAM}, and conductive bridge RRAM also known as \textbf{CBRAM}.

OxRAM exploits resistance switching in metal-oxide materials, examples are $Hf O_2$, $Ta_2 O_5$, $Ni O$, $Cu_x O$, $Ti O_x$ and some of the so called perovskite oxides \cite{makarov_emerging_2012, chen_review_2016, fujisaki_review_2013}.
The materials involved are already commonly used in the semiconductor industry, and RRAM shows a great compatibility with CMOS process \cite{akinaga_resistive_2010, yu_emerging_2016}. 

In OxRAM devices the conductive filament is formed by oxygen vacancies, these vacancies are introduced with the movement of oxygen atoms induced by an electrical field, additionally this process may be also aided by the diffusion of oxygen atoms due to Joule heating \cite{ando_non-volatile_2017}.

The rupture of the conductive filament is due to a redox reaction in the oxide layer \cite{makarov_emerging_2012}.

In OxRAM devices On/Off resistance is usually smaller than CBRAM, in the order of $10\times - 100\times$ \cite{yu_emerging_2016, ando_non-volatile_2017}.
However OxRAM shows better endurance than CBRAM, with values that can be higher than $10^9$ cycles.

Conductive bridge RRAM are a family of RRAM devices based on the formation of a conductive filament made of metals ions, such as Ag or Cu, formed through electrochemical reactions, in a solid state electrolyte \cite{makarov_emerging_2012, chen_review_2016, ando_non-volatile_2017}.
These metal ions are provided by one of the two electrodes as shown in Fig. \ref{fig:filament}.

CBRAM devices generally operate in a bipolar way \cite{valov_electrochemical_2011} and they show high On/Off resistance ratio ($100\times - 1000\times$).
CBRAM have usually faster switching speed with respect to OxRAM devices, but they generally show reduced endurance, with values in the order of $10^4 - 10^5$ cycles, even if higher values have been demonstrated \cite{valov_electrochemical_2011, fujisaki_review_2013, yu_emerging_2016}.

RRAM has attracted a lot of attention from both research and industry, its main advantages include its excellent compatibility with the CMOS process and the potential for great scalability.
RRAM devices have been demonstrated to scale down to sub-10 nm \cite{yu_resistive_2016}.
Moreover test chips with large capacities have been fabricated, such as a 32Gb memory based on OxRAM \cite{liu_1307mm2_2013} and a 16Gb memory based on CBRAM \cite{fackenthal_197_2014} \cite{yu_emerging_2016, chen_review_2016}.

RRAM also shows a lot of potential for high densities and high capacities, as cross-point arrays enable $4 F^2$ cell sizes, where $F$ is the feature size (e.g. 28 nm), moreover 3D stacking has been demonstrated for both unipolar and bipolar RRAM devices, and should enable even higher capacities \cite{li_nonvolatile_2017, liu_1307mm2_2013}.
A further increase in capacity could be enabled by RRAM devices supporting multi-level cells, 2-bit and 3-bit multi-level devices have been demonstrated \cite{yu_resistive_2016}.

With these characteristics, RRAM could replace FLASH at least for those applications that require faster access times, or complement it by sitting between DRAM and FLASH as a storage class memory \cite{yu_resistive_2016}.
While the theoretical switching time for RRAM could be reduced below $10 ns$, its current performance are still not on par with those of DRAM.
Moreover in order for RRAM to replace DRAM the biggest obstacle is the endurance, which is till not sufficient and would require both technological improvements as well as architectural wear-leveling techniques \cite{akinaga_resistive_2010, yu_resistive_2016}.

Other interesting applications for RRAM are also being explored, these include its use in neuro-inspired computing where RRAM cells are used as analog devices to emulate the function of synapses in neural network applications \cite{yu_emerging_2016, yu_resistive_2016}.

\section{Ferroelectric RAM}
Ferroelectric RAM commonly called \textbf{FRAM} or \textbf{FeRAM} is a type of memory that store the information under the form of a remnant polarization in a layer of ferroelectric material.
Polarization is the effect by which the dipoles present in a dielectric material, orient themselves in an anti-parallel fashion under the effect of an electric field, as shown in Fig. \ref{fig:polarization} \cite{gerardin_present_2010}. 
Normally the polarization disappears when the electric field is removed, however ferroelectric materials show a remnant polarization.

There are many materials that show these ferroelectric properties, however the only relevant ones are oxides \cite{gerardin_present_2010}.
In particular FRAM is commonly based on \emph{lead zirconate titanate} materials $Pb(Zr_x Ti_{1-x})O$, also called \textbf{PZT} \cite{gerardin_present_2010, hong_emerging_2014, boukhobza_emerging_2017}.
Other materials that are also common in FRAMs are the so called \textbf{SBT}, based on \emph{strontium bismuth tantalate} $SrBi_2Ta_2O_9$ \cite{gerardin_present_2010, hong_emerging_2014}.
Besides these materials others have also been investigated, focusing in particular on the reduction of the thickness and dimensions of the ferroelectric layer \cite{fujisaki_review_2013, hong_emerging_2014}.

\begin{figure}
    \centering
    \includegraphics[width=.5\linewidth]{NVM/img/fecap_polarization}
    \caption{Polarization of a dielectric and remnant polarization in ferroelectric materials from \cite{gerardin_present_2010}}%
    \label{fig:polarization}
\end{figure}

The cell structure of FRAM memories is similar to that of DRAM, consisting of one transistor and one capacitor (1T1C) \cite{boukhobza_emerging_2017}.
In the case of FRAM the capacitor is formed with a ferroelectric layer sandwiched between two metallic electrodes \cite{hong_emerging_2014, boukhobza_emerging_2017}
With this ferroelectric capacitor \textbf{FCAP} the information can be encoded as the two possible directions of the remnant polarization. 

This allows FRAM to retain the information in a non-volatile way, moreover, unlike DRAM, FRAM does not require any refreshing operation to retain the information \cite{boukhobza_emerging_2017}.
The typical polarization versus voltage characteristic of FRAM creates a hysteresis loop, as shown in figure Fig. \ref{fig:hysteresis}.
The Figure also shows the two remnant polarizations, $P_R$ and $-P_R$, that persist even when the applied voltage is zero.

\begin{figure}[h]
    \centering
    \includegraphics[width=.5\linewidth]{NVM/img/fram_hysteresis}
    \caption{Hysteresis loop of the polarization vs voltage curve, from \cite{gerardin_present_2010}}%
    \label{fig:hysteresis}
\end{figure}

The presence of a remnant polarization allows the FCAP to retain charge, which can be sensed in a destructive way similarly to what happens in DRAM \cite{gerardin_present_2010}.
This means that every time that a cell is read, it must also be rewritten to retain it's content.

FRAM was one of the first, among the emerging NVMs, to be transferred to production, where it has found it's place as an embedded memory for smart-cards and low-power micro-controllers \cite{fujisaki_review_2013, hong_emerging_2014}.

FRAM has performance comparable to that of DRAM with access times smaller than $55 ns$ \cite{ando_non-volatile_2017, gerardin_present_2010, boukhobza_emerging_2017}.
Moreover FRAM is the only NVM that shows high speed write operations, with write times smaller than $6 ns$ that have been demonstrated on a \emph{SBT} based device, where the speed was limited by the CMOSs \cite{hong_emerging_2014}.

FRAM also shows excellent endurance characteristics when compared with other NVMs, with usual values larger than $10^{12}, and $values as high as $10^{15}$ cycles that have been reported \cite{boukhobza_emerging_2017}.

The main drawback of FRAM is scalability, as it is reportedly difficult to scale to advanced nodes \cite{boukhobza_emerging_2017}.
In particular FRAM does not offer the same level of density of DRAM, with typical cell sizes around $\sim 22 F^2$ \cite{gerardin_present_2010, ando_non-volatile_2017}.

To achieve higher density and scalability advancements in the manufacturing technology are needed, as well as the development of 3D architectures \cite{hong_emerging_2014, ando_non-volatile_2017}.

Beyond the DRAM like capacitor cell, ferroelectric materials are being researched to develop new types of memory cells.
Other types of cells include architectures such vertical multi-level capacitor cell, as the one described in a recent patent \cite{frougier_multi-level_2020}.
Another class of ferroelectric memories are the ones based on \emph{transistor-type} cells, where the ferroelectric layer replaces the oxide in a MOS-like structure \cite{fujisaki_review_2013, chen_review_2016}.
These types of ferroelectric memories based on field effect transistors, are called \emph{Ferroelectric-FET} or \textbf{FeFET} memories \cite{chen_review_2016}.
Although FeFETs have demonstrated fast access times $\sim 20 ns$, and promise good scalability \cite{chen_review_2016}, they are the least mature technology among the emerging NVMs.
Moreover FeFETs currently suffer from severe limitations  when it comes to endurance, which is in the order of $10^4-10^5$ cycles, and show pour retention \cite{fujisaki_review_2013, chen_review_2016}.

%    \begin{itemize}
%      \item Memory cell similar to DRAM, but based on a ferroelectric capacitor
%      \item Store information in the polarization of the ferroelectric material (PZT)
%      \item One of the most mature NVM technology (already in the market) 
%      \item Still difficult to scale below 130 nm (90 nm for MSP430FR)
%    \end{itemize}

\section{Comparison}
\begin{figure}
    \center
    \includegraphics[width=\linewidth]{NVM/img/paper_export}
    \caption{Comparison of different NVM chips from \cite{chen_review_2016}}
    \label{fig:comparison}
\end{figure}

\input{./NVM/nvm_table.tex}
% Add comments about the lack of consistency between tools an papers 
% Add my numbers from NVSim and comment them wrt papers.
% High level model
